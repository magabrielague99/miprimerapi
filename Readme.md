# MI API FULL.
## Recursos
- Node.js
- Express.js
- MongoDB Atlas
- JWT
- Swagger
- Bcrypt
- Joi
- EC2
- Router 53
- S3
- ElastiCache
- passport

## Region para usuarios America del Norte
Region Norte de Virginia


## Ejecucion
#### 1. Lanzar instancia
```
Seleccionr AMI-Sprint3
```
#### 3. Correr nginx
```
sudo nginx
```
#### 4. Correr servidor
```
pm2 start ecosystem.config.js --env local
```

## A tener en cuenta

La base de datos se llena con unos datos semilla, en donde se llenan los productos, los usuarios por defecto y los metodos de pago.

Los ejemplos en las ordenes se pueden usar para el usuario cliente, para otras pruebas con otros usuarios, los ids de las direcciones se deben de cambiar.

El test se encuentra en la carpeta tests dentro de src.

En la sección métodos de pago, el endpoint **get Métodos de Pago** se encuentra abierto tanto para clientes como para los administradores, ya que estos dos son los que deben de tener la posibilidad de ver los métodos pago, no solo los administradores


## Documentation 
[Documentation](https://sprint3.tk/api-docs/) Se puede acceder en https://sprint3.tk/api-docs/

Usuarios:
```
| Usuario        | Contraseña  ||Rol          |
|----------------|-------------||-------------|
| admin          | 1234        ||Administrador|
| juanPc         | 1234a       ||Usuario      |
```

## Inicio de session
[Inicio de Session](https://sprint3.tk/) Se puede acceder en https://sprint3.tk/

## Metodos de pago 
[Metodos de pago ](https://sprint3.tk/pago.html) Se puede acceder en https://sprint3.tk/pago.html

Para realizar un pago se debe comprar como minimo un producto.

Usuarios:
```
| Usuario                                       | Contraseña     | metodo    |
|-----------------------------------------------|----------------|-----------|
| sb-twhyr12481195@personal.example.com         | K=l0a-m)       | paypal    |
| test_user_30795076@testuser.com               | qatest4460     | Mercapago |
```

#### Docker 
Para ejecutar el proyecto a través de docker compose utilice el siguiente codigo:
```
docker-compose up -d
```


**\*Inicie sesión en la documentación de swagger con nombre de usuario y contraseña*