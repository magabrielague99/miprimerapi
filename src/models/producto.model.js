const mongoose = require('mongoose');

const productoModelo = mongoose.Schema({
    nombreProducto: {
        type: String,
        require: true
    },
    precioProducto: {
        type: Number,
        require: true
    }
})

module.exports= mongoose.model('producto', productoModelo);
