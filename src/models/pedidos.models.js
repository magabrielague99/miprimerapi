const mongoose = require('mongoose');

const pedidosModel = mongoose.Schema({
    medioPago: {
        type: String,
        required: true
    },
    direccion: {
        type: String,
        require: true
    },
    nombreCompleto: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
    },
    telefono: {
        type: String,
        require: true
    },
    totalCuenta: {
        type: Number,
        require: true
    },
    productos: [{
        idProducto: {
            type: String,
            required: true
        },
        cantidadProducto: {
            type: Number,
            required: true
        }
    }],
    estadoPedido: {
        type: String,
        default: 'pendiente'
    }
})
module.exports = mongoose.model('pedido', pedidosModel);