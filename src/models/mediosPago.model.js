const mongoose = require('mongoose');

const medioPagoModelo = mongoose.Schema({
    nombreMedioPago: {
        type: String,
        required: true
    }
})

module.exports= mongoose.model('medioPago', medioPagoModelo)