const mongoose = require('mongoose');

const usuarioModel = new mongoose.Schema({
    nombreUsuario: {
        type: String,
        required: true
    },
    nombreCompleto: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    telefono: {
        type: Number,
        required: true
    },
    contraseña: {
        type: String,
        required: true
    },
    direccion: {
        type: String,
        required: true
    },
    activo: {
        type: Boolean,
        default: true,
    },
    esAdmin: {
        type: Boolean,
        default: false
    },
});

module.exports = mongoose.model('usuario', usuarioModel);
