const mongoose = require('mongoose');
const { configuracion } = require('./config');
const Usuario = require('../models/usuario.model');
const bcrypt = require('bcrypt');
const MedioPago = require('../models/mediosPago.model');
const Producto = require('../models/producto.model');
const Pedido = require('../models/pedidos.models');

(async () => {
    try {
        const db = await mongoose.connect(configuracion.ConeccionBD,
            { useNewUrlParser: true, useUnifiedTopology: true })
        console.log("Conectado a la base de datos", db.connection.name);
        const usuariosRegistrados = await Usuario.find();
        if (usuariosRegistrados.length == 0) {
            const usuarioNormal = new Usuario({
                _id: "612fdb74820ebd2828f4ce5f",
                nombreUsuario: "juanPc",
                nombreCompleto: "Juang Pablo Castro",
                email: "juanPc@gmail.com",
                telefono: 3189009800,
                contraseña: bcrypt.hashSync('1234a', 10),
                direccion: "calle 7 # 9-87",
                esAdmin: false,
                activo: true
            });
            await usuarioNormal.save();
            const usuarioAdmin = new Usuario({
                _id: "612f9ae1b2f0952de08f8b16",
                nombreUsuario: "admin",
                nombreCompleto: "adminstrador base datos",
                email: "admin@gmail.com",
                telefono: 31800000,
                contraseña: bcrypt.hashSync('1234', 10),
                direccion: "calle 8 admin",
                esAdmin: true,
                activo: true
            });
            await usuarioAdmin.save();
        }
        const medioPagoRegistrado = await MedioPago.find();
        if (medioPagoRegistrado.length == 0) {
            const medioPago1 = new MedioPago({
                _id: "613a6772153a371de85c1d0c",
                nombreMedioPago: "Efectivo",
            })
            await medioPago1.save();
            const medioPago2 = new MedioPago({
                _id: "613a67f3153a371de85c1d11",
                nombreMedioPago: "Tarjeta",
            })
            await medioPago2.save();
        }
        const productosRegistrados = await Producto.find();
        if (productosRegistrados.length == 0) {
            const producto1 = new Producto({
                _id: "613a820a32dd224144b8be0f",
                nombreProducto: 'Hamburguesa',
                precioProducto: 8000,
            });
            await producto1.save();
            const producto2 = new Producto({
                _id: "613a82b232dd224144b8be14",
                nombreProducto: 'Gaseosa',
                precioProducto: 2000,
            });
            await producto2.save();
        }
        const pedidosRegistrados = await Pedido.find();
        if (pedidosRegistrados.length == 0) {
            const pedidoNuevo = new Pedido({
                _id: "613a84923545062f48006215",
                productos: [
                    {
                        _id: "613a84923545062f48006216",
                        idProducto: "613a820a32dd224144b8be0f",
                        cantidadProducto: 1
                    },
                    {
                        _id: "613a84923545062f48006217",
                        idProducto: "613a82b232dd224144b8be14",
                        cantidadProducto: 2
                    }
                ],
                totalCuenta: 30000,
                medioPago: "Efectivo",
                direccion: "calle 8 admin",
                nombreCompleto: "adminstrador base datos",
                email: "admin@gmail.com",
                telefono: "31800000",
                estadoPedido: "pendiente",
            });
            await pedidoNuevo.save();
        }
    }
    catch (error) {
        console.error('Conexion a la BD fallida: ', error.message);
    }
}
)();
