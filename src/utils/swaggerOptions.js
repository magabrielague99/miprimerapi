const swaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Sprint project 4",
            version: "3.0.0",
            description: "Proyecto 4 para acamica DWBE"
        },
        servers: [
            {
                url: "https://api.sprint3.tk/",
                description: "Online Server"
            }
        ],
        components: {
            securitySchemes: {
                bearerAuth:{
                    type: "http",
                    scheme: "bearer",
                    bearerFormat: "JWT"
                }
            }
        },
        security: [
            {
                bearerAuth: []
            }
        ]
    },
    apis: ["src/routes/*.js"]
};

module.exports = swaggerOptions;
