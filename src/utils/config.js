configuracion = {
    Contrasenia: process.env.DBContraseniaSegura,
    Puerto: process.env.DBPuerto,
    ConeccionBD: process.env.DBConeccion,
    redis: process.env.RedisConeccion
};

module.exports = { configuracion }
