const express = require("express");
const router = express.Router();
const validarDatosLogin = require("../../middlewares/validarDatosLogin.middleware");
const { IniciarSesion } = require("../../controllers/usuario.controllers");
const google = require("./google");
const facebook = require("./facebook");
const linkedin = require("./linkedin");
const github = require("./github")

router.post("/Login", validarDatosLogin, IniciarSesion);

router.get("/failed", (req, res) => res.send("Hay un error en el login"));

router.use("", google);

router.use("", facebook);

router.use("", linkedin);

router.use("", github);

module.exports = router;
