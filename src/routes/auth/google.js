const express = require('express');
const router = express.Router();
const passport = require('passport');
const strategy_name = 'google';
require('dotenv').config()

router.get(`/${strategy_name}/auth`, passport.authenticate(strategy_name, { session:false,  scope: ['profile', 'email'] }));

router.get(`/${strategy_name}/callback`, 
  passport.authenticate(strategy_name, { session:false, failureRedirect: '/failed' }),
  function(req, res) {
    
    console.log(`Peticion get /${strategy_name}/callback `);
    const data = req.user._json;
    res.redirect(301, process.env.URL_doc);

  });

module.exports = router;
