const express = require('express');
const router = express.Router();
const {agregarPedido, modificarContidadProducto, eliminarProductoPedido, agregarProductoPedido, obtenerPedidosUsuario,
        cambiarEstado, obtenerPedidos, cambiarEstadoUsuario}= require('../controllers/pedido.controller');
const estaPendiente = require('../middlewares/estaPediente.middlegare');
const  esAdmin= require('../middlewares/esAdmin.middleware');
const validarDatosAgregar = require('../middlewares/validarDatosPedido.middleware');
const productoExiste = require('../middlewares/ProductoExiste.middelware');
const productoExistePedido = require('../middlewares/productoExistePedido.middleware');
const medioPagoExiste = require('../middlewares/medioPagoPedidoExiste.middelware');
const pedidoAgregar = require('../middlewares/PedidoAgregar.middlegare');
const pedidoYProductoExisten = require('../middlewares/pedidoYProductoExiste.middleware');
const validarDatosModificarCantidad = require('../middlewares/ValidarDatosModificarCantidad.middleware');
const validarDatosAgrearProducto = require('../middlewares/validarDatosAgregarProducto.middleware');
const productoNoAgregado = require('../middlewares/productoNoAgregado.middleware');
const pedidoExiste = require('../middlewares/pedidoExiste.middleware');
const estaActivo = require('../middlewares/estaActivo.middleware');

router.use(estaActivo);

/**
 * @swagger
 * /pedidos:
 *  get:
 *      summary: Obtener todos los pedidos realizados por el usuario
 *      tags: [Pedidos]
 *      responses:
 *          200:
 *              description: Lista de productos, lista de medios de pago y lista de pedidos realizados por el usuario
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/ObtenerPedido'
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 */
router.get('/', obtenerPedidosUsuario);


/**
 * @swagger
 * /pedidos/obtenerTodosPedidos:
 *  get:
 *      summary: Obtener todos los pedidos  en el sistema
 *      tags: [Pedidos]
 *      responses:
 *          200:
 *              description: Lista de pedidos del sistema
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/ObtenerPedido'
 *          203:
 *              description: Ruta solo para administrador
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 */
router.get('/obtenerTodosPedidos',esAdmin, obtenerPedidos);

/**
 * @swagger
 * /pedidos/agregarPedido:
 *  post:
 *      summary: Crea un pedido en el sistema
 *      tags: [Pedidos]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/AgregarPedido'
 *      responses:
 *          201:
 *              description: Producto creado
 *          400:
 *              description: campos vacios o prodcuto no encontrado o medio de pago no aceptado 
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 */
router.post('/agregarPedido',validarDatosAgregar, pedidoAgregar, productoExistePedido, medioPagoExiste, agregarPedido)


/**
 * @swagger
 * /pedidos/modificarCantidadProducto/{idPedido}/{idProducto}:
 *  put:
 *      summary: Modifica la cantidad de producto en el pedido
 *      tags: [Pedidos]
 *      parameters:
 *          - in: path
 *            name: idPedido
 *            required: true
 *          - in: path
 *            name: idProducto
 *            required: true
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/ModificarCantidaProducto'
 *      responses:
 *          201:
 *              description: Producto creado
 *          400:
 *              description: campos vacios o prodcuto no encontrado o medio de pago no aceptado 
 *          401:
 *              description: Usuario aun no ha iniciado sesion 
 */
router.put('/modificarCantidadProducto/:idPedido/:idProducto', validarDatosModificarCantidad ,estaPendiente, pedidoYProductoExisten, modificarContidadProducto);

/**
 * @swagger
 * /pedidos/modificarEliminarProductoPedido/{idPedido}/{idProducto}:
 *  delete:
 *      summary: elimina un producto de la lista en el pedido
 *      tags: [Pedidos]
 *      parameters:
 *          - in: path
 *            name: idPedido
 *            required: true
 *          - in: path
 *            name: idProducto
 *            required: true
 *      responses:
 *          200:
 *              description: medio de pago eliminado
 *          404:
 *              description: campos vacios o prodcuto no encontrado o medio de pago no aceptado 
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 */
router.delete('/modificarEliminarProductoPedido/:idPedido/:idProducto',estaPendiente, pedidoYProductoExisten, eliminarProductoPedido)

/**
 * @swagger
 * /pedidos/modificarAgregarProductoPedido/{idPedido}:
 *  post:
 *      summary: Modifica la cantidad de producto en el pedido
 *      tags: [Pedidos]
 *      parameters:
 *          - in: path
 *            name: idPedido
 *            required: true
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/modficarAgregarProductoPedido'
 *      responses:
 *          400:
 *              description: campos vacios o prodcuto no encontrado o medio de pago no aceptado 
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 */
router.post('/modificarAgregarProductoPedido/:idPedido', validarDatosAgrearProducto, estaPendiente, productoNoAgregado, agregarProductoPedido)

/**
 * @swagger
 * /pedidos/modificarEstado/{idPedido}:
 *  put:
 *      summary: Modifica el estado del pedido
 *      tags: [Pedidos]
 *      parameters:
 *          - in: path
 *            name: idPedido
 *            required: true
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/modificarEstado'
 *      responses:
 *          201:
 *              description: Producto creado
 *          400:
 *              description: campos vacios o prodcuto no encontrado o medio de pago no aceptado 
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403:
 *              description: Ruta solo para administradores
 */
router.put('/modificarEstado/:idPedido', esAdmin, pedidoExiste, cambiarEstado)

/**
 * @swagger
 * /pedidos/modificarEstadoUsuario/{idPedido}:
 *  put:
 *      summary: Modifica el estado del pedido
 *      tags: [Pedidos]
 *      parameters:
 *          - in: path
 *            name: idPedido
 *            required: true
 *      responses:
 *          201:
 *              description: Producto creado
 *          400:
 *              description: campos vacios o prodcuto no encontrado o medio de pago no aceptado 
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403:
 *              description: Ruta solo para administradores
 */
router.put('/modificarEstadoUsuario/:idPedido', pedidoExiste, estaPendiente, cambiarEstadoUsuario)

/**
 * @swagger
 * tags:
 *  name: Pedidos
 *  description: Seccion de pedidos 
 * 
 * components: 
 *  schemas:
 *      AgregarPedido:
 *          type: object
 *          required:
 *              -Productos
 *              -estadoPedido
 *              -medioPago
 *          properties:
 *              Productos:
 *                  type: array
 *                  description: Lusta de productos de un usuario  
 *              estadoPedido:
 *                  type: string
 *                  description: estado del pedido
 *                  schema:
 *                      $ref: '#/components/schemas/estadoPedido'
 *              medioPago:
 *                  type: string
 *                  descrption: medio de pago del pedido
 *          example:
 *              productos:
 *                      [
 *                          {
 *                                  "idProducto": 613a820a32dd224144b8be0f,
 *                                  "cantidadProducto": 3       
 *                           },
 *                           {
 *                                  "idProducto": 613a82b232dd224144b8be14,
 *                                  "cantidadProducto": 3
 *                            }    
 *                      ]
 *              idMedioPago: 613a67f3153a371de85c1d11   
 *      estadoPedido:
 *          type: string
 *          enum:
 *              - pediente
 *              - completo
 *      ModficarIdPedido:
 *          type: object
 *          required:
 *              -idPedido
 *          properties:
 *              idPedido:
 *                  type: integer
 *                  description: Id del pedido
 *      ModificarIdProducto:
 *          type: object
 *          required:
 *              -idProducto
 *          properties:
 *              idProducto:
 *                  type: integer
 *                  description: Id del producto 
 *      ModificarCantidaProducto:
 *          type: object
 *          required:
 *              -cantidadProducto
 *          properties:
 *              cantidadProducto:
 *                  type: integer
 *                  description: cantidad de producto 
 *          example:  
 *              cantidadProducto: 4
 *      modficarAgregarProductoPedido:
 *          type: object
 *          required:
 *              -idProducto
 *              -cantidadProducto
 *          properties:
 *              idProducto:
 *                  type: integer
 *                  description: identificador del producto
 *              cantidadProducto:
 *                  type: integer
 *                  description: cantidad de producto 
 *          example: 
 *              "idProducto": "613a82b232dd224144b8be14"
 *              "cantidadProducto": 2   
 *      ObtenerPedido:
 *          type: object
 *          required:
 *              -IdProducto
 *              -nombreProducto
 *              -precio
 *              -IdMedioPago
 *              -nombreMedioPago
 *              -IdPedido
 *              -productos
 *              -totalCuenta
 *              -direccioin
 *              -medioPago
 *              -estadoPedido
 *          properties:
 *              IdPedido:
 *                  type: number
 *                  descrption: Numero de pedido
 *              productos:
 *                  type: array
 *                  description: Lista de productos de un usuario  
 *              direccion:
 *                  type: string
 *                  description: direccion de envio del pedido
 *              totalCuenta:
 *                  type: integer
 *                  description: Valor total de la cuenta
 *              estadoPedido:
 *                  type: string
 *                  description: estado del pedido
 *              medioPago:
 *                  type: string
 *                  descrption: medio de pago del pedido
 *          example:  
 *              Productos:
 *                  {
 *                      idProducto: 613a82b232dd224144b8be14,
 *                      nombrePdocuto: Hamburguesa,
 *                      precio: 8000
 *                  }
 *              idpedido: 613a820a32dd224144b8be0i 
 *              "Total  de la Cuenta" : 20000,
 *              "Direccion" : calle 8 N° 5-93,
 *              "Medio pago" : Efectivo,
 *              "Estado pedido" : Confirmado                                 
 *      modificarEstado:
 *          type: object
 *          required:
 *              -estadoPedido
 *          properties:
 *              estadoPedido:
 *                  type: string
 *                  description: estado del producto 
 *          example:  
 *              estadoPedido: Enviado
 */
module.exports=router;