const express = require('express');
const router = express.Router();
const {agregarProducto, modificarProducto,  eliminarProducto, obtenerProductos}= require('../controllers/producto.controller');
const productoNoRepetido = require('../middlewares/productoNoRepetido.middleware')
const productoExiste = require('../middlewares/ProductoExiste.middelware');
const validarDatosProducto = require('../middlewares/validarDatosProducto.middleware');
const esAdmin = require('../middlewares/esAdmin.middleware');
//const cacheProductos = require('../middlewares/cacheProductos.middleware');
const estaActivo = require('../middlewares/estaActivo.middleware');

router.use(estaActivo);

/**
 * @swagger
 * /productos/:
 *  get:
 *      summary: Obtener todos los productos  del sistema
 *      tags: [Productos]
 *      responses:
 *          200:
 *              description: Lista de prodcutos del sistema
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Agregar'
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403: 
 *              description: Ruta solo para administradores
 */
router.get('/', obtenerProductos);

router.get('/paymet',obtenerProductos);

router.use(esAdmin);

/**
 * @swagger
 * /productos/{idProducto}:
 *  delete:
 *      summary: elimina un producto en el sistema
 *      tags: [Productos]
 *      parameters:
 *          - in: path
 *            name: idProducto
 *            schema:
 *                 $ref: '#/components/schemas/Modificar'
 *            required: true
 *      responses:
 *          200:
 *              description: Producto eliminado
 *          400:
 *              description: Peticion no valida
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403: 
 *              description: Ruta solo para administradores
 */
router.delete('/:idProducto', productoExiste,  eliminarProducto)

router.use(validarDatosProducto);

/**
 * @swagger
 * /productos:
 *  post:
 *      summary: Crea un producto en el sistema
 *      tags: [Productos]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Agregar'
 *      responses:
 *          201:
 *              description: Producto creado
 *          400:
 *              description: peticion no valida
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403: 
 *              description: Ruta solo para administradores
 */
router.post('/',productoNoRepetido,agregarProducto);

/**
 * @swagger
 * /productos/{idProducto}:
 *  put:
 *      summary: Modifica un producto en el sistema
 *      tags: [Productos]
 *      parameters:
 *          - in: path
 *            name: idProducto
 *            schema:
 *                $ref: '#/components/schemas/Modificar'
 *            required: true
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Agregar'
 *      responses:
 *          200:
 *              description: Producto modificado
 *          400:
 *              description: Peticion no valida
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403: 
 *              description: Ruta solo para administradores
 */
router.put('/:idProducto', productoExiste, modificarProducto)

/**
 * @swagger
 * tags:
 *  name: Productos
 *  description: Seccion de metodos de producto 
 * 
 * components: 
 *  schemas:
 *      Agregar:
 *          type: object
 *          required:
 *              -nombreProducto
 *              -precio
 *          properties:
 *              nombreProducto:
 *                  type: string
 *                  description: Nombre del producto  
 *              precio:
 *                  type: integer
 *                  description: Precio del producto 
 *          example:  
 *              nombreProducto: papas fritas
 *              precioProducto: 4000
 *      Modficar:
 *          type: object
 *          required:
 *              -idProducto
 *          properties:
 *              idProducto:
 *                  type: integer
 *                  description: Id del producto 
 *              
 */
module.exports=router;
