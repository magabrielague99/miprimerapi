const express = require("express");
const router = express.Router();
require("dotenv").config();
const mercadopago = require("mercadopago");

// Agrega credenciales
mercadopago.configure({
  access_token: process.env.MERCADOPAGO_TOKEN,
});

router.get("/sucess", (req, res) => {
  res.send("success");
});

router.get("/failure", (req, res) => {
  res.send("failure");
});

router.get("/pending", (req, res) => {
  res.send("pending");
});

router.post("/pago", function (req, res) {
  console.log("New request POST to /pago");
  // TODO: protect this route with a middleware

  // TODO: get user data from the database
  const user = {
    id: 1059359827,
    name: "TESTOY0C2ZNA",
    password: "qatest6807",
    site_status: "active",
    email: "test_user_88753197@testuser.com",
  };

  // TODO: get items from the database
  // Crea un objeto de preferencia
  let preference = {
    auto_return: "approved",
    back_urls: {
      success: `${process.env.URL_FRONT}/mercadoPago/success`, // TODO: define this
      failure: `${process.env.URL_FRONT}/mercadoPago/failure`, // TODO: define this
      pending: `${process.env.URL_FRONT}/mercadoPago/pending`, // TODO: define this
    },
    payer: {
      name: user.name,
      surname: user.last_name,
      email: user.email,
    },
    items: req.body.items,
  };

  // petición a mercado pago para preparar la compra
  mercadopago.preferences
    .create(preference)
    .then(function (response) {
      // Ok, haga el proceso de pago con este id:
      console.log(response);
      let id = response.body.id;
      res.json({ preference_id: id, url: response.body.sandbox_init_point });
    })
    .catch(function (error) {
      console.log(error);
    });
});

module.exports = router;
