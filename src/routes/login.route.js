const express = require('express');
const router = express.Router();
const { agregarUsuario, IniciarSesion, suspenderUsuario, obtenerUsuarios} = require('../controllers/usuario.controllers');
const usuarioNoRepetido = require('../middlewares/usuarioNoRepetido.middleware');
const validarDatosRegistro = require('../middlewares/ValidarDatosCrearUsuario.middleware');
const validarDatosLogin = require('../middlewares/validarDatosLogin.middleware');
const usuarioExiste = require('../middlewares/usuarioExste.middleware');
const esAdmin = require('../middlewares/esAdmin.middleware');

/**
 * @swagger
 * /Suspender:
 *  put:
 *      summary: Suspende usuario en el sistema
 *      tags: [Login]
 *      parameters:
 *          - in: path
 *            name: idUsuario
 *      responses:
 *          200:
 *              description: Usuario suspendido
 *          400:
 *              descriptcion: Peticion invalida
 */
router.put('/Suspender/:idUsuario', esAdmin, usuarioExiste, suspenderUsuario)

/**
 * @swagger
 * /Registro:
 *  post:
 *      summary: Crea un usuario en el sistema
 *      security: []
 *      tags: [Login]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/registro'
 *      responses:
 *          201:
 *              description: Usuario creado
 *          404:
 *              description: Peticion invalida
 */
router.post('/Registro', usuarioNoRepetido, validarDatosRegistro, agregarUsuario)

/**
 * @swagger
 * /Login:
 *  post:
 *      summary: Verifica si el usuario logeo correctamente
 *      security: []
 *      tags: [Login]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/login'
 *      responses:
 *          200:
 *              description: el usuario logeado correctamete
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/login'
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 */
router.post('/Login', validarDatosLogin, IniciarSesion)



/**
 * @swagger
 * tags:
 *  name: Login
 *  description: Seccion de Registro de usuario
 * 
 * components: 
 *  schemas:
 *      registro:
 *          type: object
 *          required:
 *              -nombreUsuario
 *              -nombreCompleto
 *              -email
 *              -telefono
 *              -direccion
 *              -contrasena
 *          properties:
 *              nombreUsuario:
 *                  type: string
 *                  description: nombre de usuario 
 *              nombreCompleto:
 *                  type: string
 *                  description: nombre completo del usuario
 *              email:
 *                  type: string
 *                  description: Email del usuario
 *              telefono: 
 *                  type: integer
 *                  description: numero de telefono del usuario
 *              direccion:
 *                  type: string
 *                  description: direccion del usuaruio
 *              contrasena:
 *                  type: string
 *                  description: Contrasena del usuario
 *          example:  
 *                  nombreUsuario: juan34
 *                  nombreCompleto: juan camilo rosas
 *                  email: juan34f@gmail.com
 *                  telefono: 3170987654   
 *                  direccion: calle 57 N° 8-97
 *                  contraseña: 12345a    
 *                  repetirContraseña: 12345a                               
 *      login:
 *          type: object
 *          required:
 *              -nombreUsuario
 *              -contrasenia
 *          properties:
 *              nombreUsuario:
 *                  type: string
 *                  description: nombre de usuario 
 *              contrasenia:
 *                  type: string
 *                  description: Contrasena del usuario
 *          example:  
 *              nombreUsuario: juan34
 *              contrasenia: 12345a             
 */
module.exports = router;
