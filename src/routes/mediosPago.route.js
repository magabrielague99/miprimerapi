const express = require('express');
const router = express.Router();
const validarDatos= require('../middlewares/ValidarDatosMedioPago.middleware');
const {obtenermediosPago, agregarMedioPago, modificarMedioPago, eliminarMedioPago}= require('../controllers/mediosPago.controllers');
const medioPagoNoRepetido = require('../middlewares/medioPagoNoRepetido.middleware');
const medioPagoExiste = require('../middlewares/medioPagoExsite.middleware');
const esAdmin = require('../middlewares/esAdmin.middleware');
const estaActivo = require('../middlewares/estaActivo.middleware');

router.use(estaActivo);

/**
 * @swagger
 * /mediosPago:
 *  get:
 *      summary: Obtener todos los medios de pago  del sistema
 *      tags: [mediosPago]
 *      responses:
 *          200:
 *              description: Lista de medios de pago del sistema
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/AgregarMedioPago'
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403: 
 *              description: Ruta solo para administradores
 */
router.get('/', obtenermediosPago);

router.use(esAdmin);

/**
 * @swagger
 * /mediosPago/{idMedioPago}:
 *  delete:
 *      summary: elimina un medio de pago en el sistema
 *      tags: [mediosPago]
 *      parameters:
 *          - in: path
 *            name: idMedioPago
 *            schema:
 *                 $ref: '#/components/schemas/ModificarMedioPago'
 *            required: true
 *      responses:
 *          200:
 *              description: medio de pago eliminado
 *          400:
 *              description: Peticion incorrecta
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403: 
 *              description: Ruta solo para administradores
 */
router.delete('/:idMedioPago', medioPagoExiste, eliminarMedioPago);

router.use(validarDatos);

/**
 * @swagger
 * /mediosPago:
 *  post:
 *      summary: Crea un medio de pago en el sistema
 *      tags: [mediosPago]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/AgregarMedioPago'
 *      responses:
 *          201:
 *              description: medio de pago creado
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403: 
 *              description: Ruta solo para administradores
 */
router.post('/', medioPagoNoRepetido, agregarMedioPago);

/**
 * @swagger
 * /mediosPago/{idMedioPago}:
 *  put:
 *      summary: Modifica un medio de pago en el sistema
 *      tags: [mediosPago]
 *      parameters:
 *          - in: path
 *            name: idMedioPago
 *            schema:
 *                $ref: '#/components/schemas/ModificarMedioPago'
 *            required: true
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/ModficarMedioPago'
 *      responses:
 *          200:
 *              description: Nombre medio de pago Modificado
 *          400:
 *              description: Peticion Incorrecta
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403: 
 *              description: Ruta solo para administradores
 */
router.put('/:idMedioPago', medioPagoExiste, medioPagoNoRepetido,  modificarMedioPago);

/**
 * @swagger
 * tags:
 *  name: mediosPago
 *  description: Seccion de metodos de medio de pago 
 * 
 * components: 
 *  schemas:
 *      AgregarMedioPago:
 *          type: object
 *          required:
 *              -nombreMedioPago
 *          properties:
 *              nombreMedioPago:
 *                  type: string
 *                  description: Nombre del medio de pago  
 *          example: 
 *              nombreMedioPago: "Cheque"
 *      ModficarMedioPago:
 *          type: object
 *          required:
 *              -idMedioPAgo
 *          properties:
 *              idMedioPago:
 *                  type: integer
 *                  description: Id del medio de pago 
 *          example: 
 *              nombreMedioPago: "Cheque2"
 */
module.exports=router;