const chai = require('chai');
const chaiHttp = require('chai-http');
const jsonwebtoken = require('jsonwebtoken');
const bcrypt = require('bcrypt');
require('dotenv').config({ path: './.env' })
const Usuario = require('../models/usuario.model');
const app = require('../index');

chai.should();
chai.use(chaiHttp);

describe('Registro de usuario', () => {
    describe('POST /Registro de forma exitosa', () => {
        it('debe devolver un 201 en status', (done) => {
            const usuario = {
                nombreUsuario: "test",
                nombreCompleto: "test test tes",
                email: "test@test.com",
                telefono: 31800000000,
                contraseña: '12345',
                repetirContraseña: '12345',
                direccion: "direccion test"
            };
            chai.request(app)
                .post('/Registro')
                .send(usuario)
                .end((err, response) => {
                    response.should.have.status(201);
                    response.should.be.an('object');
                    done();
                });
        });

        after(async () => {
            await Usuario.deleteOne({ email: 'test@test.com' });
        });
    });

    describe('POST /Registro de forma fallida el body', () => {
        it('debe devolver un 404 en status porque el nombre de usuario tiene menos de 3 caracteres', (done) => {
            const usuario = {
                nombreUsuario: "t",
                nombreCompleto: "test test tes",
                email: "test@test.com",
                telefono: 31800000000,
                contraseña: '12345',
                repetirContraseña: '12345',
                direccion: "direccion test"
            };
            chai.request(app)
                .post('/Registro')
                .send(usuario)
                .end((err, response) => {
                    response.should.have.status(404);
                    done();
                });
        });

        it('debe devolver un 404 en status porque nombre completo de usuario tiene menos de 5 caracteres', (done) => {
            const usuario = {
                nombreUsuario: "test",
                nombreCompleto: "tes",
                email: "test@test.com",
                telefono: 31800000000,
                contraseña: '12345',
                repetirContraseña: '12345',
                direccion: "direccion test",
            };
            chai.request(app)
                .post('/Registro')
                .send(usuario)
                .end((err, response) => {
                    response.should.have.status(404);
                    done();
                });
        });

        it('debe devolver un 404 en status porque el email no es .com .net', (done) => {
            const usuario = {
                nombreUsuario: "test",
                nombreCompleto: "test test tes",
                email: "test@test.co",
                telefono: 31800000000,
                contraseña: '12345',
                repetirContraseña: '12345',
                direccion: "direccion test"
            };
            chai.request(app)
                .post('/Registro')
                .send(usuario)
                .end((err, response) => {
                    response.should.have.status(404);
                    done();
                });
        });

        it('debe devolver un 404 en status porque no coincide las contrasenas', (done) => {
            const usuario = {
                nombreUsuario: "test",
                nombreCompleto: "test test tes",
                email: "test@test.com",
                telefono: 31800000000,
                contraseña: '12345',
                repetirContraseña: '12345A',
                direccion: "direccion test"
            };
            chai.request(app)
                .post('/Registro')
                .send(usuario)
                .end((err, response) => {
                    response.should.have.status(404);
                    done();
                });
        });

        it('debe devolver un 404 en status porque la direccion es menor de 5 caracteres ', (done) => {
            const usuario = {
                nombreUsuario: "test",
                nombreCompleto: "test test tes",
                email: "test@test.com",
                telefono: 31800000000,
                contraseña: '12345',
                repetirContraseña: '12345',
                direccion: "dir"
            };
            chai.request(app)
                .post('/Registro')
                .send(usuario)
                .end((err, response) => {
                    response.should.have.status(404);
                    done();
                });
        });

        it('debe devolver un 404 en status porque telefono no es un numero', (done) => {
            const usuario = {
                nombreUsuario: "test",
                nombreCompleto: "tes",
                email: "test@test.com",
                telefono: "31800000000",
                contraseña: '12345',
                repetirContraseña: '12345',
                direccion: "direccion test",
            };
            chai.request(app)
                .post('/Registro')
                .send(usuario)
                .end((err, response) => {
                    response.should.have.status(404);
                    done();
                });
        });
        
    });

    describe('POST /Registro de forma fallida por usuario duplicado', () => {
        before(async () => {
            const usuario = new Usuario({
                nombreUsuario: "test",
                nombreCompleto: "test test tes",
                email: "test@test.com",
                telefono: 31800000000,
                contraseña: '12345',
                repetirContraseña: '12345',
                direccion: "direccion test"
            });
            await usuario.save();
            token = jsonwebtoken.sign({
                email: 'test@test.co',
                esAdministrador: false,
            }, 'QZHa0Ye4v7B4ttCtV6OedhyoLXX0C');
        });
    
        it('debe devolver un 400 en status por nombreUsuario duplicado ', (done) => {
            const usuario = {
                nombreUsuario: "test",
                nombreCompleto: "test test tes",
                email: "test@testin.com",
                telefono: 31800000000,
                contraseña: '12345',
                repetirContraseña: '12345',
                direccion: "direccion test"
            };
            chai.request(app)
                .post('/Registro')
                .send(usuario)
                .end((err, response) => {
                    response.should.have.status(201);
                    response.should.be.an('object');
                    done();
                });
        });

        it('debe devolver un 400 en status por email duplicado ', (done) => {
            const usuario = {
                nombreUsuario: "testing",
                nombreCompleto: "test test tes",
                email: "test@test.com",
                telefono: 31800000000,
                contraseña: '12345',
                repetirContraseña: '12345',
                direccion: "direccion test"
            };
            chai.request(app)
                .post('/Registro')
                .send(usuario)
                .end((err, response) => {
                    response.should.have.status(201);
                    response.should.be.an('object');
                    done();
                });
        });

        after(async () => {
            await Usuario.deleteOne({ email: 'test@test.com' });
        });
    });

});