const Joi = require('joi');

const pedidoSchema = Joi.object({
    idMedioPago: Joi.string()
        .min(5)
        .max(50)
        .required(),

    productos: Joi.array().items(Joi.object({
        idProducto: Joi.string()
            .min(5)
            .max(50)
            .required(),
        cantidadProducto: Joi.number()
            .min(1)
            .required()
    })),
    estadoPedido: Joi.string()

})

module.exports = pedidoSchema;
