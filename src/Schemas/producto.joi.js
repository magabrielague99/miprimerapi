const Joi = require('joi');

const productoSchema = Joi.object({
    nombreProducto: Joi.string()
        .min(3)
        .max(30)
        .required(),
        
    precioProducto: Joi.number()
        .required()
});

module.exports = productoSchema;