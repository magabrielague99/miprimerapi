const Joi = require('joi');

const modificarCantidadSchema = Joi.object({
    cantidadProducto: Joi.number()
        .min(1)
        .required(),
})

module.exports = modificarCantidadSchema;