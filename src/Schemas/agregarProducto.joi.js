const Joi = require('joi');

const agregarProductoSchema = Joi.object({
    idProducto: Joi.string()
        .min(5)
        .max(50)
        .required(),
    cantidadProducto: Joi.number()
        .min(1)
        .required(),
})

module.exports = agregarProductoSchema;