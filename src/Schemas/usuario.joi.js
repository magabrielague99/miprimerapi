const Joi = require('joi');

const usuarioSchema = Joi.object({
    nombreUsuario: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),

    nombreCompleto: Joi.string()
        .min(5)
        .max(50)
        .required(),

    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
        .required(),

    telefono: Joi.number()
        .required(),

    contraseña: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
        .required(),

    repetirContraseña: Joi.ref('contraseña'),

    direccion: Joi.string()
        .min(5)
        .max(60)
        .required(),

    activo: Joi.boolean(),
    esAdmin: Joi.boolean()
})

module.exports= usuarioSchema;
