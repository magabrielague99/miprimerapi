const cambiarEstadoSchema = Joi.object({
    cambiarEstado: Joi.String()
        .permiter('En Preparacion', 'Enviado', 'Entregado'),
        .required(),
})

module.exports = cambiarEstadoSchema;