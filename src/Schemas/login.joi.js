const Joi = require('joi');

const loginSchema = Joi.object({
    nombreUsuario: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),

    contrasenia: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
        .required()

})

module.exports = loginSchema;