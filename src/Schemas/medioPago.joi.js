const Joi = require('joi');

const medioPagoSchema = Joi.object({
    nombreMedioPago: Joi.string()
        .min(3)
        .max(30)
        .required()
});

module.exports= medioPagoSchema;