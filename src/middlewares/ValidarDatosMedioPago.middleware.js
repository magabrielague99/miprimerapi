const medioPagoSchema = require('../Schemas/medioPago.joi');

const medioPagoDatosCorrectos = async (req, res, next) => {
    try {
        await medioPagoSchema.validateAsync(req.body);
        next();
    } catch (error) {
        console.error(error.details[0].message);
        res.status(404).json(error.details[0].message)
    }
};

module.exports = medioPagoDatosCorrectos;