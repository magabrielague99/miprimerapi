const Pedido = require('../models/pedidos.models');

const estaPediente = async (req, res, next) => {
    try {
        const { idPedido } = req.body;
        const estadoPendiente = await Pedido.findOne({ idPedido }, { estadoPedido: 'Pendiente' });
        if (estadoPendiente != null) next();
        else {
            console.log("Error: 400, El pedido ya fue confirmado");
            res.status(404).json("El pedido ya fue confirmado");
        }
    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message)
    }

};

module.exports = estaPediente;