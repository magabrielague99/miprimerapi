const crearUsuarioSchema = require('../Schemas/usuario.joi');

const crearUsuarioDatosCorrectos = async (req, res, next) => {
    try {
        await crearUsuarioSchema.validateAsync(req.body);
        next();
    } catch (e) {
        console.error(e.details[0].message);
        res.status(404).json(e.details[0].message);
    }
};

module.exports = crearUsuarioDatosCorrectos;