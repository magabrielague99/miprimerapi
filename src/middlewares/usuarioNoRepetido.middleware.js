const Usuario = require('../models/usuario.model');

const usuarioNoRepetido = async (req, res, next)=> {
    try{
        const {email, nombreUsuario }= req.body;
        const resultadoEmail = await Usuario.findOne({email});
        const resultadoNombreUsuario = await Usuario.findOne({nombreUsuario})
        if( resultadoEmail == null || resultadoNombreUsuario == null) next();
        else{
            console.error("Usuario ya registrado");
            res.status(404).json("Usuario ya Registrado")
        }
    }catch(e){
        console.error(e.message);
        res.status(404).json(e.message);
    }
}

module.exports = usuarioNoRepetido;