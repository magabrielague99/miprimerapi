const Producto = require('../models/producto.model');

const productoDuplicado = async (req, res, next) => {
    try {
        const { nombreProducto} = req.body;
        const resultado = await Producto.findOne({ nombreProducto });
        if (resultado == null) next();
        else{
            console.log('Producto ya regstrado');
            res.status(404).json('Producto ya registrado')
        }
    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message);
    }
}

module.exports= productoDuplicado;