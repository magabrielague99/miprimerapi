const Pedido = require('../models/pedidos.models')

const pedidoYProductoExiste = async (req, res, next) => {
    try {
        const { idPedido, idProducto } = req.params;
        const pedidoEncotrado = await Pedido.findById(idPedido);
        if(pedidoEncotrado !=null) {
            const productos = pedidoEncotrado.productos;
            const productoEncotrado = productos.findIndex(prod => prod.idProducto == idProducto);
            console.log(productos);
            if(productoEncotrado >= 0 ) next()
            else{
                console.error('Producto ya encuentra en Pedido, modifiue cantidad');
                res.status(404).json('Producto ya encuentra en Pedido, modifique cantidad')
            }
        }
        else{
            console.error('Pedido no encontrado');
            res.estatus(404).json('Pedido no encontrado')
        }
    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message);
    }
}

module.exports = pedidoYProductoExiste;