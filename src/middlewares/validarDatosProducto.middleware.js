const prodcutoSchema = require('../Schemas/producto.joi');

const productoDatosCorrecto = async (req, res, next)=>{
    try{
        await prodcutoSchema.validateAsync(req.body);
        next();
    } catch(e){
        console.error(e.details[0].message);
        res.status(404).json(e.details[0].message);
    }
}

module.exports= productoDatosCorrecto;