const CambiarEstadoSchema = require('../Schemas/cambiarEstado.joi');

const CambiarEstadoDatosCorrectos = async (req, res, next) => {
    try {
        await CambiarEstadoSchema.validateAsync(req.body);
        next();
    } catch (e) {
        console.error(e.details[0].message);
        res.status(404).json(e.details[0].message);
    }
};

module.exports = CambiarEstadoDatosCorrectos;