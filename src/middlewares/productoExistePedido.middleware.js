const Producto = require('../models/producto.model');

const productoExistePedido = async (req, res, next) => {
    try {
        const { productos } = req.body;
        await Promise.all(productos.map(async(producto)=>{
             await Producto.findById(producto.idProducto);
        }));
        next();
    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message);
    }
}

module.exports = productoExistePedido;