const {rdbc} = require('../utils/redis');

const cacheProductos =  async (req, res, next) => {
    console.log(rdbc)
     const productos= await rdbc.get('productos');
        if (productos) {
            res.json(productos);
        } else {
            next();
        }
}

module.exports = cacheProductos;
