const modificarCantidadSchema = require('../Schemas/modificarCantidad.joi')

const modificarCantidadDatosCorrectos = async (req, res, next) => {
    try {
        await modificarCantidadSchema.validateAsync(req.body);
        next();
    } catch (error) {
        console.error(error.details[0].message);
        res.status(404).json(error.details[0].message)
    }
};

module.exports = modificarCantidadDatosCorrectos;