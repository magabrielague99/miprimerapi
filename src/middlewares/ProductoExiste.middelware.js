const Producto = require('../models/producto.model');

const productoExiste = async (req, res, next)=>{
    try{
        const { idProducto } = req.params;
        const productoEncontrado = await Producto.findById(idProducto);
        if(productoEncontrado != null)   next();
        else{
            console.error("Producto no encontrado");
            res.status(404).json("Producto no encontrado")
        }
    }catch(e){
        console.error(e.message);
        res.status(404).json(e.message);
    }
}

module.exports= productoExiste;