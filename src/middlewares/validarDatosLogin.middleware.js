const loginSchema = require('../Schemas/login.joi')

const loginDatosCorrectos = async (req, res, next) => {
    try {
        await loginSchema.validateAsync(req.body);
        next();
    } catch (error) {
        console.error(error.details[0].message);
        res.status(404).json(error.details[0].message)
    }
};

module.exports = loginDatosCorrectos;