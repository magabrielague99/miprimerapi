const Pedido = require('../models/pedidos.models')

const pedidoExiste= async (req, res, next)=>{
    try {
        const { idPedido }= req.params;
        const pedidoEncontrado = await Pedido.findById(idPedido);
        if (pedidoEncontrado != null) next();
        else {
            console.error("Pedido no encontrado");
            res.status(400).json("Pedido no encontrado");
        }
    } catch(e){
        console.error(e.message);
        res.status(404).json(e.message)
    }
};

module.exports= pedidoExiste;