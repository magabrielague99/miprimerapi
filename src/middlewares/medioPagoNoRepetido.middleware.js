const MedioPago = require('../models/mediosPago.model')

const medioPagoNoRepetido = async (req, res, next) => {
    const { nombreMedioPago } = req.body;
    try {
        const resultado = await MedioPago.findOne({ nombreMedioPago });
        if (resultado == null) next();
        else {
            console.log("Medio de pago ya registrado");
            res.status(404).json("Medio de pago ya registrado")
        }
    }
    catch (e) {
        console.error(e.message);
        res.status(404).json(e.message)
    }
};

module.exports = medioPagoNoRepetido;