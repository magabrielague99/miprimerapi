const Usuario = require('../models/usuario.model');
const Pedido = require('../models/pedidos.models');

const estaPediente = async (req, res, next) => {
    try {
        const { nombreUsuario } = req.user;
        const usuarioEncontrado = await Usuario.findOne({ nombreUsuario });
        const estadoPendiente = await Pedido.findOne({ nombreCompleto: usuarioEncontrado.nombreCompleto }, { estadoPedido: 'pendiente' });
        if (estadoPendiente == null) next();
        else {
            console.log("Error: 400, Hay un pedido Pendiente");
            res.status(404).json("Hay un pedido pendiente");
        }
    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message)
    }

};

module.exports = estaPediente;