const Usuario = require('../models/usuario.model')

const estaActivo = async (req, res, next) => {
    try {
        const { nombreUsuario } = req.user;
        const usuarioEncontrado = await Usuario.findOne({ nombreUsuario });
        if (usuarioEncontrado.activo) next();
        else {
            console.log("Error: 404, El usuario no esta activo");
            res.status(404).json("El usuario no esta activo")
        };
    } catch (e) {
        console.log(e.message);
        res.status(404).json(e.message)
    }
};
module.exports = estaActivo;