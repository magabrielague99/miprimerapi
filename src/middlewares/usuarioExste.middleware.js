const Usuario = require('../models/usuario.model');

const usuarioExiste = async (req, res, next)=> {
    try{
        const {idUsuario} = req.params;
        const usuarioEncontado = await Usuario.findById(idUsuario);
        if( usuarioEncontado != null ) next();
        else{
            console.error("Usuario no encontrado");
            res.status(404).json("Usuario no encontrado")
        }
    }catch(e){
        console.error(e.message);
        res.status(404).json(e.message);
    }
}

module.exports = usuarioExiste;