const MedioPago = require('../models/mediosPago.model')

const medioPagoExisteEnPedido= async (req, res, next)=>{
    try {
        const { idMedioPago }= req.body;
        const medioPagoEncontrado = await MedioPago.findById(idMedioPago);
        if (medioPagoEncontrado) next();
        else {
            console.error("Medio de Pago no encontrado");
            res.status(400).json("Medio de Pago no encontrado");
        }
    } catch(e){
        console.error(e.message);
        res.status(404).json(e.message)
    }
};

module.exports= medioPagoExisteEnPedido;