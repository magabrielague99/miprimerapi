const pedidoSchema = require('../Schemas/pedido.joi');

const pedidoDatosCorrectos = async (req, res, next) => {
    try {
        await pedidoSchema.validateAsync(req.body);
        next();
    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message);
    }
};

module.exports = pedidoDatosCorrectos;