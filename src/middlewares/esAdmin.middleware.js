const Usuario = require('../models/usuario.model')

const esAdmin = async (req, res, next) => {
    try {
        const { nombreUsuario } = req.user;
        const usuarioEncontrado = await Usuario.findOne({ nombreUsuario });
        if (usuarioEncontrado.esAdmin) next();
        else {
            console.log("Error: 403, Ruta solo para administradores");
            res.status(403).json("Ruta solo para administradores")
        };
    } catch (e) {
        console.log(e.message);
        res.status(404).json(e.message)
    }

};

module.exports = esAdmin;