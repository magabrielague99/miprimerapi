/* eslint-disable linebreak-style */
const express = require("express");
const cors = require("cors");
//const helmet = require("helmet");
const expressJwt = require("express-jwt");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUI = require("swagger-ui-express");
require("dotenv").config({ path: "./.env" });
const loginRoutes = require("./routes/login.route");
const productoRoutes = require("./routes/producto.route");
const mediosPagoRoutes = require("./routes/mediosPago.route");
const pedidosRoutes = require("./routes/pedidos.route");
const auth_routes = require("./routes/auth");
const payment_routes = require("./routes/payment");
//const direccionRoutes = require('./routes/direccion.route');
const swaggerOptions = require("./utils/swaggerOptions");
const { configuracion } = require("./utils/config");
const passport = require("passport");
require("./utils/BaseDatos");
require("./service");

const app = express();

app.use(cors());
//app.use(helmet());
app.use(passport.initialize());

app.use(express.json());

const swaggerSpecs = swaggerJsDoc(swaggerOptions);

app.use(auth_routes);

app.use(payment_routes);

app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerSpecs));

app.use(
  expressJwt({
    secret: configuracion.Contrasenia,
    algorithms: ["HS256"],
  }).unless({
    path: ["/Login", "/Registro"],
  })
);

app.use("/", loginRoutes);

app.use((err, req, res, _next) => {
  if (err.name === "UnauthorizedError") {
    res.status(401).json("Token invalido");
  } else {
    res.status(500).json("Internal server error");
  }
});

app.use("/pedidos", pedidosRoutes);

app.use("/productos", productoRoutes);

app.use("/mediosPago", mediosPagoRoutes);

app.listen(configuracion.Puerto, () => {
  console.log(`Corriendo en el puerto ${configuracion.Puerto}`);
});

module.exports = app;
