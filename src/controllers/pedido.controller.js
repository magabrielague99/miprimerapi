const Pedido = require('../models/pedidos.models');
const Producto = require('../models/producto.model');
const Usuario = require('../models/usuario.model');
const MedioPago = require('../models/mediosPago.model');

const obtenerPedidos = async (req, res) => {
    try {
        const pedidos = await Pedido.find()
        console.log(pedidos);
        res.status(200).json(pedidos)
    } catch (e) {
        console.log(e.message);
        res.status(404).json(e.message);
    }
}

const agregarPedido = async (req, res) => {
    try {
        const { productos, idMedioPago } = req.body
        const nombreUsuario = req.user.nombreUsuario;
        const totalCuenta = await calcularTotalPagar(productos);
        const usuarioEncontrado = await Usuario.findOne({ nombreUsuario });
        const medioPago = await MedioPago.findById(idMedioPago);
        const pedidoNuevo = new Pedido({
            productos,
            totalCuenta,
            medioPago: medioPago.nombreMedioPago,
            direccion: usuarioEncontrado.direccion,
            nombreCompleto: usuarioEncontrado.nombreCompleto,
            email: usuarioEncontrado.email,
            telefono: usuarioEncontrado.telefono
        });
        const resultado = await pedidoNuevo.save();
        console.log(resultado);
        res.status(201).json(resultado);
    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message);
    }
}

const modificarContidadProducto = async (req, res) => {
    try {
        const { idPedido, idProducto } = req.params;
        const { cantidadProducto } = req.body;
        const pedidoEncontrado = await Pedido.findById(idPedido)
        const productos = pedidoEncontrado.productos;
        const resultado = productos.find(prod => prod.idProducto == idProducto)
        if (resultado) {
            resultado.cantidadProducto = cantidadProducto;
            const totalCuenta = await calcularTotalPagar(productos);
            pedidoEncontrado.productos = productos,
                pedidoEncontrado.totalCuenta = totalCuenta,
                pedidoEncontrado.medioPago = pedidoEncontrado.medioPago,
                pedidoEncontrado.direccion = pedidoEncontrado.direccion,
                pedidoEncontrado.nombreCompleto = pedidoEncontrado.nombreCompleto,
                pedidoEncontrado.email = pedidoEncontrado.email,
                pedidoEncontrado.telefono = pedidoEncontrado.telefono
            await pedidoEncontrado.save()
            console.log(pedidoEncontrado);
            res.status(200).json(pedidoEncontrado)
        } else {
            console.error('producto no encotrado');
            res.status(404).json('producto no encotrado')
        }
    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message);
    }
}

const eliminarProductoPedido = async (req, res) => {
    const { idPedido, idProducto } = req.params;
    try {
        const pedidoEncontrado = await Pedido.findById(idPedido)
        const productos = pedidoEncontrado.productos;
        const resultado = productos.findIndex(prod => prod.idProducto == idProducto)
        if (resultado >= 0) {
            productos.splice(resultado, 1);
            const totalCuenta = await calcularTotalPagar(productos);
            pedidoEncontrado.productos = productos,
                pedidoEncontrado.totalCuenta = totalCuenta,
                pedidoEncontrado.medioPago = pedidoEncontrado.medioPago,
                pedidoEncontrado.direccion = pedidoEncontrado.direccion,
                pedidoEncontrado.nombreCompleto = pedidoEncontrado.nombreCompleto,
                pedidoEncontrado.email = pedidoEncontrado.email,
                pedidoEncontrado.telefono = pedidoEncontrado.telefono
            await pedidoEncontrado.save()
            console.log(pedidoEncontrado);
            res.status(200).json(pedidoEncontrado)
        } else {
            console.error('producto no encotrado');
            res.status(404).json('producto no encotrado')
        }
    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message);
    }
}

const agregarProductoPedido = async (req, res) => {
    try {
        const { idPedido } = req.params;
        const { cantidadProducto, idProducto } = req.body;
        const pedidoEncontrado = await Pedido.findById(idPedido)
        const productoEncontrado = await Producto.findById(idProducto);
        if (productoEncontrado) {
            const productos = pedidoEncontrado.productos;
            productoNuevo = {
                idProducto: idProducto,
                cantidadProducto: cantidadProducto
            }
            productos.push(productoNuevo);
            const totalCuenta = await calcularTotalPagar(productos);
            pedidoEncontrado.productos = productos,
                pedidoEncontrado.totalCuenta = totalCuenta,
                pedidoEncontrado.medioPago = pedidoEncontrado.medioPago,
                pedidoEncontrado.direccion = pedidoEncontrado.direccion,
                pedidoEncontrado.nombreCompleto = pedidoEncontrado.nombreCompleto,
                pedidoEncontrado.email = pedidoEncontrado.email,
                pedidoEncontrado.telefono = pedidoEncontrado.telefono
            await pedidoEncontrado.save()
            console.log(pedidoEncontrado);
            res.status(200).json(pedidoEncontrado)
        } else {
            console.error("Producto no encontrado");
            res.status(404).json("Producto no encontrado");
        }

    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message);
    }
}


const calcularTotalPagar = async (productos) => {
    let totalCuenta = 0;
    try {
        await Promise.all(productos.map(async (producto) => {
            const productoEncontrado = await Producto.findById(producto.idProducto);
            totalCuenta += productoEncontrado.precioProducto * producto.cantidadProducto;
        }));
        return totalCuenta;
    } catch (e) {
        console.error(e.message);
        return e.message
    }
}

const obtenerPedidosUsuario = async (req, res) => {
    try {
        const { nombreUsuario } = req.user;
        const usuarioEncontrado = await Usuario.findOne({ nombreUsuario })
        if (usuarioEncontrado != null) {
            const pedidoUsuario = await Pedido.find({ nombreCompleto: usuarioEncontrado.nombreCompleto })
            console.log(pedidoUsuario);
            res.status(200).json(pedidoUsuario);
        }
    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message);
    }
}

const cambiarEstado = async (req, res) => {
    try {
        const { idPedido } = req.params;
        const { estadoPedido } = req.body;
        const pedidoEncontrado = await Pedido.findById(idPedido);
        if (pedidoEncontrado) {
            pedidoEncontrado.productos = pedidoEncontrado.productos,
                pedidoEncontrado.totalCuenta = pedidoEncontrado.totalCuenta,
                pedidoEncontrado.medioPago = pedidoEncontrado.medioPago,
                pedidoEncontrado.direccion = pedidoEncontrado.direccion,
                pedidoEncontrado.nombreCompleto = pedidoEncontrado.nombreCompleto,
                pedidoEncontrado.email = pedidoEncontrado.email,
                pedidoEncontrado.telefono = pedidoEncontrado.telefono,
                pedidoEncontrado.estadoPedido = estadoPedido
            await pedidoEncontrado.save()
            console.log(pedidoEncontrado);
            res.status(200).json(pedidoEncontrado)
        } else {
            console.error('pedido no encotrado');
            res.status(404).json('pedido no encotrado')
        }
    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message);
    }
}

const cambiarEstadoUsuario = async (req, res) => {
    try {
        const { idPedido } = req.params;
        const estadoPedido = "Confirmado";
        const pedidoEncontrado = await Pedido.findById(idPedido);
        if (pedidoEncontrado) {
            pedidoEncontrado.estadoPedido = estadoPedido,
                pedidoEncontrado.productos = pedidoEncontrado.productos,
                pedidoEncontrado.totalCuenta = pedidoEncontrado.totalCuenta,
                pedidoEncontrado.medioPago = pedidoEncontrado.medioPago,
                pedidoEncontrado.direccion = pedidoEncontrado.direccion,
                pedidoEncontrado.nombreCompleto = pedidoEncontrado.nombreCompleto,
                pedidoEncontrado.email = pedidoEncontrado.email,
                pedidoEncontrado.telefono = pedidoEncontrado.telefono,
                await pedidoEncontrado.save()
            console.log(pedidoEncontrado);
            res.status(200).json(pedidoEncontrado)
        } else {
            console.error('pedido no encotrado');
            res.status(404).json('pedido no encotrado')
        }
    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message);
    }
}

module.exports = {
    obtenerPedidos, agregarPedido, modificarContidadProducto, eliminarProductoPedido, agregarProductoPedido,
    obtenerPedidosUsuario, cambiarEstado, cambiarEstadoUsuario
}