const Producto = require('../models/producto.model')
const { rdbc } = require('../utils/redis');

const obtenerProductos=async (req, res)=>{
    try{
        const productos= await Producto.find();
        rdbc.set('productos', 60 * 1, JSON.stringify(productos));
        console.log(productos);
        res.status(200).json(productos);
    }catch(e){
        console.error(e.message);
        res.status(404).json(e.message);
    }
}

const agregarProducto = async (req, res) => {
    try{
        const {nombreProducto, precioProducto} = req.body;
        const productoNuevo = new Producto ({
            nombreProducto,
            precioProducto
        });
        await productoNuevo.save();
        console.log("Medio de pago agregado",productoNuevo);
        res.status(201).json(productoNuevo);
    } catch(e){
        console.error(e.message);
        res.status(404).json(e.message);
    }
}

const modificarProducto = async (req, res) => {
    try{
        const { idProducto }= req.params;
        const { nombreProducto, precioProducto} = req.body;
        const productoModificar = await Producto.findById(idProducto);
        if(productoModificar){
            productoModificar.nombreProducto = nombreProducto;
            productoModificar.precioProducto = precioProducto;
            productoModificar.save();
            clientRedis.setex('productos', 1, JSON.stringify(''));
            console.log("Producto actualizado");
            res.status(200). json("Producto actualizado");
        }
    }catch(e){
        console.error(e.message);
        res.status(404).json(e.message);
    }
}

const eliminarProducto= async (req, res)=>{
    try{
        const {idProducto} = req.params;
        await Producto.findByIdAndDelete(idProducto);
        console.log("Producto eliminado");
        res.status(200).json("Producto eliminado")
    }catch(e){
        console.error(e.message);
        res.status(404).json(e.message);
    }
}

module.exports={agregarProducto, modificarProducto, obtenerProductos, eliminarProducto}
