const Usuario = require("../models/usuario.model");
const bcrypt = require("bcrypt");
const jsonwebtoken = require("jsonwebtoken");

const agregarUsuario = async (req, res) => {
  try {
    const {
      nombreUsuario,
      nombreCompleto,
      email,
      telefono,
      contraseña,
      repetirContraseña,
      direccion,
      activo,
      esAdmin,
    } = req.body;
    const usuarioNuevo = new Usuario({
      nombreUsuario,
      nombreCompleto,
      email,
      telefono,
      contraseña: bcrypt.hashSync(contraseña, 10),
      repetirContraseña,
      direccion,
      activo,
      esAdmin,
    });
    await usuarioNuevo.save();
    console.log("Usuario Creado", usuarioNuevo);
    res.status(201).json(usuarioNuevo);
  } catch (e) {
    console.error(e.message);
    res.status(404).json(e.message);
  }
};

const obtenerUsuarios = async (req, res) => {
  try {
    const usuarios = await Usuario.find();
    console.log(usuarios);
    res.status(200).json(usuarios);
  } catch (e) {
    console.error(e.message);
    res.status(404).json(e.message);
  }
};

const IniciarSesion = async (req, res) => {
  try {
    const { nombreUsuario, contrasenia } = req.body;
    const usuario = await Usuario.findOne({ nombreUsuario });
    console.log(usuario.contraseña);
    if (usuario.activo) {
      const resultado = bcrypt.compareSync(contrasenia, usuario.contraseña);
      if (resultado) {
        const token = jsonwebtoken.sign(
          {
            nombreUsuario,
          },
          configuracion.Contrasenia
        );
        console.log({ token });
        res.json({ token });
      } else {
        console.error("Usuario no atorizado, datos incorrectos");
        res.status(401).json("Unauthorized");
      }
    } else {
      console.log("Error: 404, El usuario no esta activo");
      res.status(404).json("El usuario no esta activo");
    }
  } catch (e) {
    res.status(404).json(e.message);
  }
};

const suspenderUsuario = async (req, res) => {
  try {
    const { idUsuario } = req.params;
    const usuarioEncontrado = await Usuario.findById(idUsuario);
    if (usuarioEncontrado && usuarioEncontrado.activo) {
      usuarioEncontrado.nombreUsuario,
        usuarioEncontrado.nombreCompleto,
        usuarioEncontrado.email,
        usuarioEncontrado.telefono,
        usuarioEncontrado.contraseña,
        usuarioEncontrado.direccion,
        (usuarioEncontrado.activo = false),
        usuarioEncontrado.esAdmin;
      await usuarioEncontrado.save();
      console.log("usuario", usuarioEncontrado);
      res.status(200).json(usuarioEncontrado);
    } else {
      console.log("Error: 404, El usuario no esta activo");
      res.status(400).json("El usuario no esta activo");
    }
  } catch (e) {
    console.log(e.message);
    res.status(404).json(e.message);
  }
};

module.exports = {
  agregarUsuario,
  obtenerUsuarios,
  IniciarSesion,
  suspenderUsuario,
};
