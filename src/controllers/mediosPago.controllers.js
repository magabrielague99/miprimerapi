const MedioPago = require('../models/mediosPago.model')

const obtenermediosPago = async (req, res) => {
    try {
        const mediosPago = await MedioPago.find()
        console.log(mediosPago);
        res.status(200).json(mediosPago)
    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message);
    }

};

const agregarMedioPago = async (req, res) => {
    try {
        const { nombreMedioPago } = req.body;
        const medioPagoNuevo = new MedioPago({
            nombreMedioPago
        });
        await medioPagoNuevo.save();
        console.log("Medio de pago agregado", medioPagoNuevo);
        res.status(201).json(medioPagoNuevo);
    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message)
    }
}

const modificarMedioPago = async (req, res) => {
    const { idMedioPago } = req.params;
    const { nombreMedioPago } = req.body;
    try {
        const medioPagoModificar = await MedioPago.findById(idMedioPago);
        if (medioPagoModificar) {
            medioPagoModificar.nombreMedioPago = nombreMedioPago;
            medioPagoModificar.save();
            console.log("Medio de pago actualizado");
            res.status(200).json("Medio de pago actualizado");
        }
    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message)
    }

}

const eliminarMedioPago = async (req, res) => {
    const { idMedioPago } = req.params
    try {
        await MedioPago.findByIdAndDelete(idMedioPago);
        console.log("Medio de pago elimnado");
        res.status(200).json("Medio de pago eliminado");
    } catch (e) {
        console.error(e.message);
        res.status(404).json(e.message)
    }

}

module.exports = { agregarMedioPago, modificarMedioPago, obtenermediosPago, eliminarMedioPago }